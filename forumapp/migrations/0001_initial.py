# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Categories',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cat_name', models.CharField(max_length=500, verbose_name=b'Categories')),
                ('cat_description', models.CharField(max_length=1500, verbose_name=b'Categories Description')),
                ('pub_date', models.DateTimeField(auto_now_add=True, verbose_name=b'Publish Date')),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.CharField(max_length=500, verbose_name=b'Comment ')),
                ('comment_date', models.DateTimeField(auto_now_add=True, verbose_name=b'Comment Date')),
                ('like', models.IntegerField(default=0, verbose_name=b'Like')),
            ],
        ),
        migrations.CreateModel(
            name='Posts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('posts_content', models.CharField(max_length=500, verbose_name=b'Post Content')),
                ('posts_date', models.DateTimeField(auto_now_add=True, verbose_name=b'Post Date')),
            ],
        ),
        migrations.CreateModel(
            name='Topics',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('topic_subject', models.CharField(max_length=1500, verbose_name=b'Topic Subject')),
                ('topic_date', models.DateTimeField(auto_now_add=True, verbose_name=b'Publish Date')),
                ('categories', models.ForeignKey(to='forumapp.Categories')),
            ],
        ),
        migrations.AddField(
            model_name='posts',
            name='topics',
            field=models.ForeignKey(to='forumapp.Topics'),
        ),
        migrations.AddField(
            model_name='comment',
            name='topics',
            field=models.ForeignKey(to='forumapp.Topics'),
        ),
    ]
