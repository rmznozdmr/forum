# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('forumapp', '0002_auto_20150624_0816'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='comment',
            name='like',
        ),
        migrations.AddField(
            model_name='topics',
            name='like',
            field=models.IntegerField(default=0, verbose_name=b'Like'),
        ),
    ]
