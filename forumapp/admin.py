from django.contrib import admin
import time
from .models import Categories, Topics
from django.contrib.auth.models import User
#
# class Topics(admin.StackedInline):
#     model = Topics
#     extra = 1


class CategoriesAdmin(admin.ModelAdmin):
     model = Categories
     extra = 2




admin.site.register(Categories)
admin.site.register(Topics)