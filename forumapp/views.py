import profile
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, Http404

# Create your views here.
from django.shortcuts import render, get_object_or_404, render_to_response
from django.template import RequestContext
from django.views import generic
from django.views.generic import View
from django.contrib import auth
from forumapp.forms import CategoriesForm, TopicsForm,CommentsForm
from forumapp.models import Categories, Topics
from django.contrib.auth.models import User

from django.contrib.auth.models import User
from django import forms



class IndexView(generic.ListView):

    model = Categories
    template_name = 'index.html'
    context_object_name = 'latest_categories_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Categories.objects.order_by('pub_date')


class DetailView(generic.DetailView):
    model = Categories
    template_name = 'category.html'
    comment_form=CommentsForm()





#Results view class for votes results
class ResultsView(generic.DetailView):
    model = Categories
    template_name = 'results.html'


# Question view for list all of Questions
class CategoriesView(View):
    # get method 
    def get(self, request):
        categories_form = CategoriesForm()
        return render(request, 'create_cat.html', {
            'categories_form': categories_form,
        })

    def post(self, request):
        categories_form = CategoriesForm(request.POST)
        if categories_form.is_valid():
            categories_form.save()
            return HttpResponseRedirect(reverse('forumapp:index'))
        else:
            return render(request, 'create_cat.html', {
                'categories_form': categories_form,
                'errors': categories_form.errors
            })
class CategoriesUpdateView(View):

    def dispatch(self, request, question_id, *args, **kwargs):
        self.categories = Categories.objects.get(pk=question_id)
        return super(CategoriesUpdateView, self).dispatch(request, *args, **kwargs)
    def get(self, request):

        self.categories_form = CategoriesForm()
        return render(request, 'updateCat.html', {
            'categories_form': self.categories_form,
        })

    def post(self, request):
        self.categories_form = CategoriesForm(request.POST)
        if self.categories_form.is_valid():
            self.categories.delete()
            self.categories_form.save()
            return HttpResponseRedirect(reverse('forumapp:index'))
        else:
            return render(request, 'create_cat.html', {
                'categories_form': self.categories_form,
                'errors': self.categories_form.errors
            })
def delete(request, id):
    categories = get_object_or_404(Categories, pk=id).delete()
    return HttpResponseRedirect(reverse('forumapp:index'))


#Choice view for create a new choice
class TopicView(View):
    # get and post same part
    def dispatch(self, request, question_id, *args, **kwargs):
        self.categories = Categories.objects.get(pk=question_id)
        return super(TopicView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        choice_form = TopicsForm(initial={'categories': self.categories})
        return render(request, 'create_topic.html', {
            'categories': self.categories,
            'choice_form': choice_form,
        })

    def post(self, request, *args, **kwargs):
        choice_form = TopicsForm(request.POST, initial={'categories': self.categories})
        if choice_form.is_valid():
            choice_form.save()
            return HttpResponseRedirect(reverse('forumapp:detail', args=(choice_form.cleaned_data.get('categories').pk,)))
        else:
            return render(request, 'create_topic.html', {
                'categories': self.categories,
                'choice_form': choice_form,
                'errors': choice_form.errors
            })


class CommentView(View):
    # get and post same part
    # get method
    def get(self, request):
        comments_form = CommentsForm()
        return render(request, 'comment.html', {
            'comments_form': comments_form,
        })

    def post(self, request):
        comments_form = CommentsForm(request.POST)
        if comments_form.is_valid():
            comments_form.save()
            return HttpResponseRedirect(reverse('forumapp:index'))
        else:
            return render(request, 'comment.html', {
                'comments_form': comments_form,
                'errors': comments_form.errors
            })



def login_view(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = auth.authenticate(username=username, password=password)
    if user is not None and user.is_active:
        # Correct password, and the user is marked "active"
        auth.login(request, user)
        # Redirect to a success page.
        return HttpResponseRedirect(reverse("login"))
    else:
        # Show an error page
        return HttpResponseRedirect("/account/invalid/")

def logout_view(request):
    auth.logout(request)
    # Redirect to a success page.
    if request.method =='POST':
        return render("index.html")


def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            new_user = form.save()
            return HttpResponseRedirect("/books/")
    else:
        form = UserCreationForm()
    return render(request, "registration/register.html", {
        'form': form,
    })