__author__ = 'discovery'
__author__ = 'discovery'
from django.conf.urls import url
from django.contrib.auth.views import login, logout


from django.conf.urls import url



from . import views

# all url patterns
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    #url(r'^(?P<question_id>[0-9]+)/vote/$',views.VoteView.as_view(), name='vote'),
    url(r'^categories/$', views.CategoriesView.as_view(), name='categories'),
    url(r'^topic/(?P<question_id>\d+)/$', views.TopicView.as_view(), name='topic'),
    url(r'^comment/$', views.CommentView.as_view(), name='comment'),
    url(r'^update/(?P<question_id>\d+)/$', views.CategoriesUpdateView.as_view(), name='updatecat'),
    url(r'^delete/(?P<id>\d+)/$',views.delete,name='deletecat'),



    #url(r'^(?P<pk>[0-9]+)/vote/$', views.VotesView.as_view(), name='vote'),
]