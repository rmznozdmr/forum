import user
from django.contrib.auth.hashers import make_password
from django.shortcuts import render

# Create your views here.

from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_200_OK
from rest_framework.views import APIView
from df2 import permissions,authenticators
from django.contrib.auth.models import User
from forumapp.models import Categories, Topics,Comment
from df2.serializers import CategoriesSerializer, TopicsSerializer,CommentSerializer,UserSerializer
from rest_framework import serializers as rserializers
from django.contrib.auth import login, logout, authenticate
from rest_framework.response import Response


from rest_framework.exceptions import ParseError
from rest_framework import status



from rest_framework.authtoken.models import Token



class CategoriesViewset(viewsets.ModelViewSet):
    serializer_class = CategoriesSerializer
    permission_classes = (AllowAny,)
    queryset = Categories.objects.all()
    # def get_permissions(self):
    #      # allow non-authenticated user to get list
    #      return (AllowAny() if self.request.method == 'GET'
    #              else permissions.IsAuthenticated()),




class TopicsViewset(viewsets.ModelViewSet):
    queryset = Topics.objects.all()
    serializer_class = TopicsSerializer


    #permission_classes = (permissions.IsStaffOrTargetUser,)
    def create(self, request, *args, **kwargs):

        author = User.objects.get(pk=4)
        #if request.user.is_authenticated() request.user ekleencek :
        if True:
            serializer=TopicsSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save(user=author)
                return Response(serializer.data,HTTP_200_OK)
            else:
                return Response(serializer.error_messages,HTTP_400_BAD_REQUEST)
        else:
            return Response({"error","you are not inside"})


class CommentViewset(viewsets.ModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    #permission_classes = (permissions.IsStaffOrTargetUser,)
    def create(self, request, *args, **kwargs):

        author = User.objects.get(pk=4)
        #if request.user.is_authenticated():
        request.data['user'] = author
        if True:
            serializer=CommentSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save(user=author)
                return Response(serializer.data,HTTP_200_OK)
            else:
                return Response(serializer.error_messages,HTTP_400_BAD_REQUEST)
        else:
            return Response({"error","you are not inside"})

class UserViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (permissions.IsOwnerUser,)
    def create(self, request, *args, **kwargs):
        serializers=UserSerializer(data=request.data)
        if serializers.is_valid():

            password=make_password(serializers.validated_data.get('password'))
            serializers.save(password=password)
            return Response({"key":"Hosgeldiniz"},status=HTTP_200_OK)
        else:
            return Response( {"key":"invalid users informations"},status=HTTP_400_BAD_REQUEST)




class AuthView(APIView):
    authentication_classes = (authenticators.QuietBasicAuthentication,)

    def post(self, request, *args, **kwargs):
        login(request, request.user)
        return Response(UserSerializer(request.user).data)

    def delete(self, request, *args, **kwargs):
        logout(request)
        return Response()

class logOutView(APIView):
    def get(self,request):
        logout(request)
        return Response({"key":"Good Bye"})

class  loginView(APIView):
    def post(self,request):
        serializers=UserSerializer(data=request.data)
        username="deneme"
        password="1234"
        serializers.is_valid()
        if True:
            username = serializers.data.get('username')
            password = serializers.data.get('password')

            # Use Django's machinery to attempt to see if the username/password
            # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

            # If we have a User object, the details are correct.
            # If None (Python's way of representing the absence of a value), no user
            # with matching credentials was found.
        if user:
                # Is the account active? It could have been disabled.
            if user.is_active:
                    # If the account is valid and active, we can log the user in.
                    # We'll send the user back to the homepage.
                login(request, user)

                token = Token.objects.get_or_create(user=user)

                return Response({'detail': 'POST answer', 'token': token[0].key,'username':user.username})

            else:
                    # An inactive account was used - no logging in!
                return Response("Your Rango account is disabled.")
        else:
                # Bad login details were provided. So we can't log the user in.
            print "Invalid login details: {0}, {1}".format(username, password)
            return Response("Invalid login details supplied.")

class LikeView(APIView):




    def get(self, request, question_id, *args, **kwargs):
        users = request.user



        serializer = self.__get_serializer_class()(data=request.data)
        if True:
            try:
                selected_choice = Topics.objects.get(pk=question_id)
            except (KeyError, Topics.DoesNotExist):
                return Response("Topic does not exist", status=HTTP_400_BAD_REQUEST)
            else:
                selected_choice.like += 1
                selected_choice.save()
                return Response({'like': selected_choice.like}, status=HTTP_200_OK)
        else:
            return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)

    def __get_serializer_class(self):
        choices = Topics.objects.all()  # However you want to restrict the choices

        class LikeSerializer(rserializers.Serializer):
            topic = rserializers.ChoiceField(choices=list([(choice.pk, choice.topic_subject) for choice in choices]))

            class Meta:
                fields = ('topic_subject','like')

        return LikeSerializer