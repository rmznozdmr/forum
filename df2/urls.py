from django.contrib.auth.decorators import login_required

__author__ = 'discovery'


from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter

from df2 import views



router = DefaultRouter()
router.register('categories', views.CategoriesViewset, base_name='categories')
router.register(r'posts', views.TopicsViewset,base_name='post')
router.register(r'comments',views.CommentViewset)
router.register(r'users',views.UserViewset)



urlpatterns = [
    url(r'^', (include(router.get_urls()))),
    url(r'^like/(?P<question_id>\d+)/$', (views.LikeView.as_view()), name="like"),
    url(r'^login/$', (views.loginView.as_view()), name="login"),
    url(r'^logout/$', (views.logOutView.as_view()), name="logout"),
    url(r'^auth/$',
        views.AuthView.as_view(),
        name='authenticate')

]