__author__ = 'discovery'
from rest_framework import permissions


class IsStaffOrTargetUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method == 'GET' or request.user.is_staff or request.user.is_authenticated()
    #admin herseyi yapabilir
    def has_object_permission(self, request, view, obj):
        return request.user.is_staff or obj.user == request.user and request.user.is_authenticated()  or request.method == 'GET'


class IsOwner(permissions.BasePermission):
    """a user can edit only their own posts"""
    def has_object_permission(self, request, view, obj):
        return request.user == obj.user

class IsAuthenticated(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.method == 'GET' or request.user.is_authenticated()

class IsOwnerUser(permissions.BasePermission):
    def has_permission(self, request, view):
        return view.action=='create' or view.action=='list'
    def has_object_permission(self, request, view, obj):
        return request.user==obj