from django.contrib.auth.models import User

__author__ = 'discovery'
from rest_framework.authentication import BasicAuthentication
from rest_framework import exceptions

class QuietBasicAuthentication(BasicAuthentication):
     def authenticate(self, request):
        username = request.META.get('X_USERNAME')
        if not username:
            return None

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise exceptions.AuthenticationFailed('No such user')

        return (user, None)