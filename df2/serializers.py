from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.validators import UniqueTogetherValidator

__author__ = 'discovery'

from rest_framework import serializers
from forumapp.models import Categories, Topics,Comment
from django.contrib.auth.models import User



class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'password','email')




class CommentSerializer(serializers.ModelSerializer):
    user=UserSerializer(many=False,read_only=True)
    class Meta:
        model = Comment
        fields = ('id','user','topics','comment','comment_date')



class TopicsSerializer(serializers.ModelSerializer):
    user =UserSerializer(read_only=True)
    #comment_set = PrimaryKeyRelatedField(queryset=Comment.objects.all())
    comment_set= CommentSerializer(many=True,read_only=True)
    class Meta:
        model = Topics
        fields = ('id',
                  'user',
                  'categories',
                  'topic_subject',
                  'topic_date',
                  'like',
                  'comment_set',
                  )





class CategoriesSerializer(serializers.ModelSerializer):
    topics_set = TopicsSerializer(many=True, read_only=True)


    class Meta:
        model = Categories
        fields = ('id', 'cat_name', 'cat_description','pub_date', 'topics_set',)

